# Stage 0
# =======
FROM node:10-alpine as build-stage

WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn install

COPY . ./
RUN yarn build

# Stage 1
# =======
FROM nginx:mainline-alpine

COPY --from=build-stage /app/build /usr/share/nginx/html
